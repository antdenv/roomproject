﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameOfButton : MonoBehaviour
{
    public GameObject button;

    void Start()
    {
        button.GetComponentInChildren<Text>().text = "Cube";
    }
}
