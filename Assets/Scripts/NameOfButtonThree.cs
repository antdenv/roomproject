﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameOfButtonThree : MonoBehaviour
{
    public GameObject button_three;

    void Start()
    {
        button_three.GetComponentInChildren<Text>().text = "Main";
    }
}
