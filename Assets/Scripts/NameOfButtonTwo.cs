﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameOfButtonTwo : MonoBehaviour
{
    public GameObject button_two;

    void Start()
    {
        button_two.GetComponentInChildren<Text>().text = "Sphere";
    }
}
